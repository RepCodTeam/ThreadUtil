/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testhilos;

/**
 *
 * @author fespinosa
 */
public final class MilleniumFalcon extends Thread  {
    private String estado;
    private boolean termine;

    public MilleniumFalcon() {
        this.setEstado("Detendio");
        this.setTermine(false);
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public boolean isTermine() {
        return termine;
    }

    public void setTermine(boolean termine) {
        this.termine = termine;
    }
    
    @Override
    public void run() {
        try {
            MilleniumFalcon.sleep(3000);
            System.out.println("El MilleniumFalcon llego");
            setEstado("MF-MOV");
            this.setTermine(true);
        } catch (InterruptedException ex) {
            System.out.println("Error al hacer Sleep: "+ex);
        }
    }
    
    
}
