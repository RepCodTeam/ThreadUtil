/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testhilos;

/**
 *
 * @author fespinosa
 */
public class Bicicleta extends Thread  {
    private String estado;
    private boolean termine;

    public Bicicleta() {
        setEstado("Detendio");
        setTermine(false);
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public boolean isTermine() {
        return termine;
    }

    public void setTermine(boolean termine) {
        this.termine = termine;
    }
    
    @Override
    public void run() {
        try {
            MilleniumFalcon.sleep(36000);
            System.out.println("La bicicleta llego");
            this.setTermine(true);
            setEstado("B-MOV");
        } catch (InterruptedException ex) {
            System.out.println("Error al hacer Sleep: "+ex);
        }
    }
}
