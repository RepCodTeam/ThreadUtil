/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testhilos;

/**
 *
 * @author fespinosa
 */
public class Orquestador {
    
    private MilleniumFalcon mf;
    private Avion av;
    private Auto au;
    private Bicicleta b;
    
    public Orquestador(){
        mf = new MilleniumFalcon();
        av = new Avion();
        au = new Auto();
        b = new Bicicleta();
    }
    
    public boolean aCorrer() throws InterruptedException{
        mf.start();
        av.start();
        au.start();
        b.start();
        while((!mf.isTermine()) || (!av.isTermine()) || (!au.isTermine()) || (!b.isTermine()) ){
            System.out.println("Bicileta: [" + b.isTermine() + "] - Auto: [" + au.isTermine()  +"]- avion: [" + av.isTermine()  +"]- MilleniumFalcon: [" + mf.isTermine()  +"]");
            Thread.sleep(1000);
            System.out.println("Espera 1 segundo");
        }
        System.out.println("Bicileta: [" + b.isTermine() + "] - Auto: [" + au.isTermine()  +"]- avion: [" + av.isTermine()  +"]- MilleniumFalcon: [" + mf.isTermine()  +"]");
        return true;
    }

    public MilleniumFalcon getMf() {
        return mf;
    }

    public void setMf(MilleniumFalcon mf) {
        this.mf = mf;
    }

    public Avion getAv() {
        return av;
    }

    public void setAv(Avion av) {
        this.av = av;
    }

    public Auto getAu() {
        return au;
    }

    public void setAu(Auto au) {
        this.au = au;
    }

    public Bicicleta getB() {
        return b;
    }

    public void setB(Bicicleta b) {
        this.b = b;
    }
}
