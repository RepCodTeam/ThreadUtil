/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testhilos;

/**
 *
 * @author fespinosa
 */
public class TestHilos {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Inicio");
        Orquestador o = new Orquestador();
        o.aCorrer();
        MilleniumFalcon mil = o.getMf();
        Avion av = o.getAv();
        Auto au = o.getAu();
        Bicicleta b = o.getB();
        System.out.println("MilleniumFalcon: " + mil.getEstado());
        System.out.println("avion: " + av.getEstado());
        System.out.println("Auto: " + au.getEstado());
        System.out.println("Bicicleta: " + b.getEstado());
        System.out.println("=========================");
        System.out.println("Fin");
    }
    
    
    
}
